# cutthecord

Modular Client Mod for Discord's Android app.

**THIS IS A WORK IN PROGRESS BRANCH! THE PATCHES ARE INCOMPLETE!**

For status updates and support on the project, join #cutthecord on https://libera.chat. WebIRC link: https://web.libera.chat/#cutthecord

**Latest supported Discord Android version:** 78.4 - Alpha (78204), released on 2021-05-31.

New patch development will be done for the latest supported version.

![A CutTheCord screenshot](https://elixi.re/t/mh3eirsy9.png)

Check out [README.md in patches folder to see what patches are available and what each of them do](patches/README.md)!

## Binaries (apk)

An F-Droid repo is available on https://fdroid.a3.pm/seabear/repo/?fingerprint=9DC9CB5FDD85D37121A5FEE99D24475F03FEA7F2EC25FB94DD51866D87933ED1

You can add that to your phone and get updates easily or just download directly from there. **Rooting is NOT needed, CutTheCord can be installed alongside official Discord and/or other CutTheCord branches.**

Feel free to ignore play protect, it's bullshit.

If you fail recaptcha, [follow this](https://gitdab.com/distok/cutthecord/issues/22#issuecomment-82).

## Building

See [BUILDING.md](BUILDING.md).

## License

- CTCCI, patchport and other scripts are AGPLv3.
- We chose to not license the patches, and are therefore "All Rights Reserved". However, you're allowed to use it to build your own version of CutTheCord, fork CutTheCord, develop your own patches etc, and we kindly ask you to send us any patches you develop that you think may be helpful. You're free to distribute binaries (apks) including CutTheCord patches as long as you give appropriate credit to the CutTheCord project.
