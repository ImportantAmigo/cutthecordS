## DisTok CutTheCord: Custom Font Instructions

This patch is a simple file replacement, so it doesn't have patch files.

Replace the ttf files in `res/font/` and preserve their names to replace fonts.

